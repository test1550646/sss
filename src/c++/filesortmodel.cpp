#include "filesortmodel.h"
#include <QDebug>

FileSortModel::FileSortModel(QObject *parent)
    : QSortFilterProxyModel{parent}
{
    this->sort(0);
    this->setDynamicSortFilter(true);
    this->sortOrder();
    this->setFilterRole(QtEx::FileDialogModel::Name);
}

FileSortModel::~FileSortModel()
{
    fileManager.deleteLater();
}

void FileSortModel::setFilterString(QString newFilter)
{
    this->setFilterFixedString(newFilter);
}

void FileSortModel::setModel(QString string, QString mask)
{
    fileManager.setMask(mask);
    this->setSourceModel(&fileManager);
    this->fileManager.setPath(string);
    this->setSortRole(QtEx::FileDialogModel::Name);
    this->sort(0, Qt::AscendingOrder);
    emit fullPathChanged(fileManager.fullPath());
}

void FileSortModel::setSortModel(int newSortFlag)
{
    if(this->sortOrder() == Qt::AscendingOrder){
        this->sort(0, Qt::DescendingOrder);
    }else{
        this->sort(0, Qt::AscendingOrder);
    }
    this->setSortRole(newSortFlag);
}

QString FileSortModel::mask()
{
    return this->fileManager.mask();
}

int FileSortModel::moveIndex(int index)
{
    return this->mapFromSource(fileManager.index(index)).row();
}

int FileSortModel::moveIndexBack(int index)
{
    return this->mapToSource(fileManager.index(index)).row();
}

bool FileSortModel::hasBeenSorted()
{
    return (this->sortOrder() == Qt::DescendingOrder) || this->sortRole() == QtEx::FileDialogModel::LastChanged;
}
