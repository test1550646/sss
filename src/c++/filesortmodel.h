#ifndef FILESORTMODEL_H
#define FILESORTMODEL_H

#include <QSortFilterProxyModel>
#include "filemanager.h"


class FileSortModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    Q_ENUM(QtEx::FileDialogModel::Roles)
    explicit FileSortModel(QObject *parent = nullptr);

    ~FileSortModel();

    Q_INVOKABLE void setFilterString(QString string);
    Q_INVOKABLE void setModel(QString string, QString mask);
    Q_INVOKABLE void setSortModel(int newFlag);
    Q_INVOKABLE QString mask();
    Q_INVOKABLE int moveIndex(int index);
    Q_INVOKABLE int moveIndexBack(int index);
    Q_INVOKABLE bool hasBeenSorted();

signals:
    void fullPathChanged(QString fullPath);

private:
    FileManager fileManager;
};

#endif // FILESORTMODEL_H
