#ifndef FILESYSTEMMODEL_H
#define FILESYSTEMMODEL_H

#include <QtExtensionsToolkit/FileDialogModel>
class FileManager : public QtEx::FileDialogModel
{
    Q_OBJECT

public:
    explicit FileManager(QObject *parent = nullptr);
    QString fullPath() const;

private:
    void scan() noexcept override;
    void setFullPath(const QString &newFullPath);

private:
    QString m_fullPath;
};

#endif // FILESYSTEMMODEL_H
