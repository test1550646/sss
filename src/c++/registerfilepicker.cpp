#include "registerfilepicker.h"
#include "filesortmodel.h"
#include <QtExtensions/QMLRegistration>



inline void initResources()
{
#if (QT_VERSION < QT_VERSION_CHECK(6, 2, 0))
    Q_INIT_RESOURCE(filePicker);
#endif
}

void FilePickerRegister::registerTypes(const char* uri, int major, int minor) noexcept
{
    initResources();
    qmlRegisterModule(uri, major, minor);

    qmlRegisterType<FileSortModel>(uri, major, minor, "FilePicker");

    qmlRegisterType(QUrl("qrc:/RouteFlyWindow.qml"), uri, major, minor, "RouteFlyWindow");
}
