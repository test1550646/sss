struct FilePickerRegister
{
    [[maybe_unused]] static void registerTypes(const char* uri = "FilePicker",
                                               int major = 1,
                                               int minor = 0) noexcept;
};

