#include "filemanager.h"
#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <cmath>

FileManager::FileManager(QObject *parent)
    : QtEx::FileDialogModel{parent}
{
    setMask("*");
    setFilters(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    setSortFlags(QDir::DirsFirst | QDir::Name | QDir::IgnoreCase);
}

void FileManager::scan() noexcept
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    m_storage.clear();
    endRemoveRows();
    Qt::Directory dir(path(), mask(), sortFlags(), filters());
    setFullPath(dir.absolutePath());
    for(const QFileInfo& item : dir.entryInfoList())
    {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_storage.emplace_back(false, item.fileName(), parseSize(item.size()), item.lastModified(), item.suffix());
        endInsertRows();
    }
}

QString FileManager::fullPath() const
{
    return m_fullPath;
}

void FileManager::setFullPath(const QString &newFullPath)
{
    m_fullPath = newFullPath;
}
