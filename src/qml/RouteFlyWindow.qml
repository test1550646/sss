import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import FilePicker 1.0
import "qrc:/qtx/FileExtensions.js" as FileExtensions

Popup{
    id: dialogWindow
    dim: true
    closePolicy: Popup.NoAutoClose | Popup.CloseOnEscape
    modal: true
    padding: 1
    background: Rectangle { border.width: 1; }
    //
    property bool isSave: true

    // Color properties
    property color mainColor: "#2F4F4F"
    property color darkMainColor: "#0F3333"
    property color backgroundScrolColor: "#74A7A7"
    property color backgroundListColor: "#82A7A7"
    property color white: "#FFFFFF"
    property color black: "#000000"

    Item{
        id: privateVar
        property string mask: ".txt"
        property string directory: ""
        property string path: ""
        property string name: ""
        property int inputIndex: -1
    }

    signal reject
    signal accept(string path)

    enter: Transition { NumberAnimation { property: "opacity"; from: 0.0; to: 1.0; duration: 400 } }
    exit: Transition { NumberAnimation { property: "opacity"; from: 1.0; to: 0.0; duration: 400 } }

    Page{
        anchors.fill: parent
        padding: 0
        leftInset: 0
        rightInset: 0
        topInset: 0
        bottomInset: 0

        header: ToolBar {
            id: toolBar
            height: 55
            contentHeight: 55
            ColumnLayout{
                width: dialogWindow.width
                Layout.preferredHeight: 24
                Layout.margins: 0
                Label{
                    Layout.fillWidth: true
                    Layout.preferredHeight: 24
                    Layout.alignment: Qt.AlignCenter
                    text: (privateVar.path === "") ? "Здесь находится текст" : privateVar.path
                    color: dialogWindow.black
                    horizontalAlignment: Qt.AlignHCenter
                }

                RowLayout {
                    Layout.margins: 0
                    ToolButton {
                        Layout.preferredWidth: dialogWindow.width * 0.73
                        Layout.preferredHeight: 24
                        text: "Имя"
                        onPressed: {
                            __model.setSortModel(259)
                            toolBar.switchIndex()
                        }
                    }
                    ToolButton {
                        Layout.preferredWidth: dialogWindow.width * 0.26
                        Layout.preferredHeight: 24
                        text: "Дата изменения"
                        onPressed: {
                            __model.setSortModel(261)
                            toolBar.switchIndex()
                        }
                    }
                }
            }
            property int tempIndex: -1
            function switchIndex(){
                if(__view.itemAtIndex(privateVar.inputIndex) !== null){
                    __view.itemAtIndex(privateVar.inputIndex).background.opacity = 0.5
                   __view.itemAtIndex(privateVar.inputIndex).colorBackground = dialogWindow.backgroundScrolColor
                }

                if(tempIndex !== -1){
                    privateVar.inputIndex = tempIndex
                    tempIndex = -1
                }

                if(__model.hasBeenSorted() || inputText.text != ""){
                    tempIndex = privateVar.inputIndex
                    privateVar.inputIndex = __model.moveIndex(privateVar.inputIndex)
                }

                if(__view.itemAtIndex(privateVar.inputIndex) !== null){
                    __view.itemAtIndex(privateVar.inputIndex).colorBackground = dialogWindow.white
                    __view.itemAtIndex(privateVar.inputIndex).background.opacity = 1
                }
            }
        }

        footer: TabButton{
            id: toolButton
            Layout.fillWidth: parent.width
            height: parent.height/24
            background: Rectangle{ color: dialogWindow.mainColor; }

            RowLayout{
                Layout.fillWidth: parent.width

                Button{
                    id: buttonSave
                    Layout.preferredWidth: toolButton.width/2.015
                    Layout.preferredHeight: toolButton.height/1.1
                    Layout.topMargin: 2
                    text: (dialogWindow.isSave) ? "Сохранить " : "Загрузить "
                    background: Rectangle{ color: dialogWindow.white; opacity: parent.pressed ? 1 : (parent.hovered) ? 0.8 : 0.5; }
                    onClicked: {
                        standardButtons.open()
                    }
                }

                Button{
                    Layout.preferredWidth: toolButton.width/2.015
                    Layout.preferredHeight: toolButton.height/1.1
                    Layout.topMargin: 2
                    text: "Отмена"
                    background: Rectangle{ color: dialogWindow.white; opacity: parent.pressed ? 1 : (parent.hovered) ? 0.8 : 0.5; }
                    onClicked: {
                        privateVar.inputIndex = -1
                        dialogWindow.reject()
                    }
                }
            }
        }

        Dialog{
            property bool isEmpty: privateVar.path === ""
            id: standardButtons
            anchors.centerIn: parent
            modal: true
            dim: true
            background: Rectangle { border.width: 1; }
            closePolicy: Popup.NoAutoClose | Popup.CloseOnEscape
            standardButtons: Dialog.Ok| Dialog.Cancel

            Text{
                text: (standardButtons.isEmpty) ? "Вы ничего не выбрали!" : ((dialogWindow.isSave) ? "Сохранить " : "Загрузить ") +  privateVar.path
            }

            Component.onCompleted: {
                standardButtons.standardButton(Dialog.Cancel).text = "Отмена"
            }

            onAccepted: {
                if(!standardButtons.isEmpty){
                    dialogWindow.accept(privateVar.path)
                    privateVar.inputIndex = -1
                }
                dialogWindow.reject()
            }
        }

        ColumnLayout{
            width: parent.width
            height: dialogWindow.isSave ? parent.height - 40 : parent.height
            Layout.margins: 0
            Rectangle {
                Layout.fillWidth: parent.width
                Layout.preferredHeight: parent.height
                clip: true
                color: dialogWindow.backgroundScrolColor
                ListView {
                    id: __view
                    height: parent.height
                    width: parent.width
                    model: FilePicker {
                        id: __model 
                        onFullPathChanged: {
                           privateVar.directory = fullPath
                        }
                    }

                    ScrollBar.vertical: ScrollBar {
                        active: true;
                        onPositionChanged: {
                            if(__view.itemAtIndex(privateVar.inputIndex) !== null && __view.itemAtIndex(privateVar.inputIndex).colorBackground !== dialogWindow.white){
                                __view.itemAtIndex(privateVar.inputIndex).colorBackground = dialogWindow.white
                                __view.itemAtIndex(privateVar.inputIndex).background.opacity = 1
                            }
                        }
                    }

                    boundsBehavior: Flickable.StopAtBounds
                    delegate: ItemDelegate {
                        id: deleg
                        width: __view.width
                        height: 24
                        background: Rectangle{ opacity: 0.5; color: (parent.hovered) ? dialogWindow.white : colorBackground; }

                        required property int index
                        required property bool isDirectory
                        required property string name
                        required property string size
                        required property date lastChanged
                        required property string extension
                        property string colorBackground: dialogWindow.backgroundScrolColor

                        function getIcon(ext, is_dir)
                        {
                            if(is_dir)
                                return "qrc:/qtx/icons/file-types/folder.svg"
                            if(FileExtensions.iconPaths[ext])
                                return FileExtensions.iconPaths[ext]
                            return "qrc:/qtx/icons/file-types/file.svg"
                        }

                        onPressed: {
                            var tempIndex = index;
                            if(__model.hasBeenSorted() || inputText.text != ""){
                                tempIndex = __model.moveIndex(tempIndex)
                            }

                            inputText.focus = false
                            if(privateVar.inputIndex === -1)
                            {
                                privateVar.inputIndex = tempIndex
                                __view.itemAtIndex(tempIndex).background.opacity = 1
                                colorBackground = dialogWindow.white
                            }
                            else if(privateVar.inputIndex !== tempIndex)
                            {
                                if(__view.itemAtIndex(privateVar.inputIndex) !== null){
                                     __view.itemAtIndex(privateVar.inputIndex).background.opacity = 0.5
                                    __view.itemAtIndex(privateVar.inputIndex).colorBackground = dialogWindow.backgroundScrolColor
                                }

                                __view.itemAtIndex(tempIndex).colorBackground = dialogWindow.white
                                __view.itemAtIndex(tempIndex).background.opacity = 1
                                privateVar.inputIndex = tempIndex
                            }
                            privateVar.path = privateVar.directory + "/" + name
                            toolBar.tempIndex = index
                        }

                        RowLayout {
                            anchors.fill: parent
                            spacing: 0
                            ToolTip.visible: hovered
                            ToolTip.text: privateVar.directory + "/" + name
                            ToolTip.delay: 1500

                            Image {
                                Layout.preferredHeight: 24
                                Layout.preferredWidth: 30
                                source: getIcon(extension, isDirectory)
                                scale: 0.8
                            }

                            Label {
                                Layout.leftMargin: 30
                                Layout.fillWidth: true
                                Layout.preferredHeight: 24
                                text: name.replace(privateVar.mask, "")
                                font {
                                    weight: Font.DemiBold
                                }
                                elide: Text.ElideRight
                            }

                            Label{
                                function convertDate(date)
                                {
                                    let d = date.toLocaleDateString(Qt.locale("ru_RU"), "dd.MM.yyyy")
                                    let t = date.toLocaleTimeString(Qt.locale("ru_RU"), "hh:mm")
                                    return `${d} ${t}`
                                }

                                Layout.preferredHeight: 24
                                Layout.preferredWidth: dialogWindow.width * 0.26
                                text: convertDate(lastChanged)
                                elide: Text.ElideRight
                            }
                        }
                    }
                }
            }
            Rectangle{
                id: inputSaveName
                Layout.fillWidth: parent.width
                Layout.margins: 0
                height: parent.height/30
                border.color: dialogWindow.black
                visible: dialogWindow.isSave
                color: inputText.focus ? dialogWindow.backgroundListColor : dialogWindow.white

                TextInput{
                    id: inputText
                    anchors.fill: parent
                    text: text
                    selectionColor: dialogWindow.mainColor
                    font.preferShaping: false
                    selectByMouse: true
                    validator: RegExpValidator { regExp: /[0-9 () A-Z a-z А-Я а-я]+/}

                    onTextChanged: {
                        if(inputSaveName.visible){
                            privateVar.path = privateVar.directory + "/" + text + privateVar.mask
                            __model.setFilterString(text)
                        }
                        if(text === ""){
                            privateVar.path = ""
                        }
                    }
                }
            }
        }
    }

    function openWindow(isSave, dirPath){
        dialogWindow.isSave = isSave
        __model.setFilterString("")
        privateVar.path = ""
        privateVar.directory = ""
        inputText.text = ""
        dialogWindow.open()
        privateVar.mask = "*" + privateVar.mask
        __model.setModel(dirPath, privateVar.mask)
        privateVar.mask = privateVar.mask.replace("*", "")
    }
}
